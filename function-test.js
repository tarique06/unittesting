const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)

const sinon = require('sinon')
const sinonChai = require('sinon-chai')
chai.use(sinonChai)

const rewire = require('rewire')

var demo =rewire ('./function');

describe ('file to be tested', ()=>{

    context('function', ()=>{
        it('adding of two number',()=>{
            expect (demo.add(1,2)).to.equal(3);            
        })
    })


    context('function callback', ()=>{
        it('testing the callback',(done)=>{
            demo.addCallback(1,2,(err,result)=>{
                expect(err).to.not.exist;
                expect(result).to.equal(3);
                done()
            })            
        })
    })

    context('Async Await function', ()=>{
        it('testing the async await callback',async ()=>{
            let result = await demo.addPromise(1,2);
            expect (result).to.equal(3);
        })

        it('testing the async await callback',async ()=>{
            await expect(demo.addPromise(1,2)).to.eventually.equal(3);
        })
    })


    context('test Double function', ()=>{
        it('should spy on logger',()=>{
            let spy = sinon.spy(console,'log')
            demo.foo();

            expect(spy.calledOnce).to.be.true;
            expect(spy).to.have.been.calledOnce;

            spy.restore()
        })
    })



    context('stub private function', ()=>{
        it ('should stub createFile' ,async()=>{
            let createStub = sinon.stub(demo,'createFile').resolves('create_stub');
            let callStub = sinon.stub().resolves('calldb_stub');

            demo.__set__('callDB', callStub);

            let result = await demo.bar('test.txt');

            expect(result).to.equal('calldb_stub');
            expect(createStub).to.have.been.calledOnce;
            expect(createStub).to.have.been.calledWith('test.txt');
            expect(callStub).to.have.been.calledOnce;
        })
    })
})