const model = (sequelize, DataTypes) => {
    var User = sequelize.define('user', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        user_name: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        gate_no: {
            type: DataTypes.STRING(100),
            allowNull: true
        },
    });
    return User;
};

module.exports = model;