var chai = require('chai')
var { match, stub, resetHistory } = require('sinon')
const proxyquire = require('proxyquire')
var expect = chai.expect;
chai.use(require('sinon-chai'));

const {
  sequelize,
  dataTypes,
  checkModelName,
  checkPropertyExists,
  makeMockModels
} = require('sequelize-test-helpers')

const model = require('./src/models/models')

describe('Checking User Model & its properties', () => {
  const User = model(sequelize, dataTypes)
  const user = new User()

  context('table name', () => {
      checkModelName(User)('user')
  });

  context('properties name', () => {
      ['id', 'user_name', 'gate_no'].forEach(checkPropertyExists(user));
  })

})

describe('Checking table element and function',() => {
    const User = { findOne: stub() }
    const mockModels = makeMockModels({ User })
    const save = proxyquire('../sentinelFunction', {
        './models': mockModels
    })
    
    const id = 1
    const data = {
        user_name: 'Tarique',
        gate_no: 12,
    }

    const fakeUser = { id, ...data, update: stub() }

    let result

    context('user does not exist', () => {
        before(async () => {
            mockModels.User.findOne.resolves(undefined)
            result = await save({ id, ...data })
        })

        after(resetHistory)

        it('called User findOne', () => {
            expect(User.findOne).to.have.been.calledWith(match({ where: { id } }))
        })

        it("didn't call user.update", ()=> {
            expect(fakeUser.update).not.to.have.been.called
        })

        it('returned null', () => {
            expect(result).to.be.null
        })
    })
})